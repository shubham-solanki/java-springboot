package com.my.springboot.controller;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import com.my.springboot.model.HelloWorld;
import com.my.springboot.service.HelloWorldService;

@Controller
@EnableAutoConfiguration
public class HelloWorldContoller {
	
	HelloWorldService service = new HelloWorldService();
	
	@RequestMapping("magic")
    @ResponseBody
    String tellMeMagicWord() {
		HelloWorld helloWorld = new HelloWorld();
        return service.showTheWorld(helloWorld);
    }
	
	@RequestMapping("/")
    @ResponseBody
    String tellMeSomeThing() {
        return "Magic Word";
    }
}
